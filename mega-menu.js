$(document).ready(function(i) {
    var r = -1,
        n = "";
    i("#menu").find("ul").find("li").each(function() {
        for (var a = i(this).text(), e = i(this).find("a").attr("href"), t = 0, s = 0; s < a.length && -1 != (t = a.indexOf("_", t)); s++) t++;
        if (level = s, level > r && ("<ul>", n += "<ul>"), level < r) {
            offset = r - level;
            for (s = 0; s < offset; s++) "</ul></li>", n += "</ul></li>"
        }
        a = a.replace(/_/gi, ""), "<li><a href='" + e + "'>" + a + "</a>", n += "<li><a href='" + e + "'>";
        for (s = 0; s < level; s++) n += "";
        n += a + "</a>", r = level
    });
    for (var a = 0; a <= r; a++) "</ul>", n += "</ul>", 0 != a && ("</li>", n += "</li>");
    i("#menuput ").html(n), i("#menuput >  ul").attr("id", "menu-main-menu").attr("class", "nav navbar-nav navbar-white paperio-default-menu"), i("#menuput ul > li > ul").parent("li").addClass("parent").addClass("menu-item-has-children dropdown"), i("#menuput ul > li > ul").map(function() {
        i(this).attr("class", "sub-menu dropdown-menu")
    })
}), $(document).ready(function() {
    $(".BlogArchive .hierarchy .hierarchy-title").map(function() {
        $(this).append("<a class='dropper dopo' href='javascript:void();'><i class=\"fa fa-angle-down\"></i></a>")
    }), $(".BlogArchive .dropper.dopo").map(function() {
        $(this).click(function() {
            $(this).closest(".archivedate").children(".hierarchy-content").slideToggle()
        }), $(this).removeClass("dopo")
    }), $("#menu-main-menu a").map(function() {
        var a = $(this).attr("href"); - 1 != (a = a.toLowerCase()).indexOf("[mega menu]") && ($(this).addClass("mega-link "), $(this).attr("href", "/search/label/" + $(this).text() + "?&max-results=" + perPage))
    }), $("#menu-main-menu ul > li > ul").parent("li").addClass("hasSubmenu "), $("#menu-main-menu .widget").attr("style", "display:block!important;"), $("#menu-main-menu a.mega-link").map(function() {
        var b = $(this),
            a = b.attr("href");
        if (a = a.toLowerCase(), b.hasClass("mega-link")) {
            var y = b.text();
            $.ajax({
                url: "/feeds/posts/default/-/" + y + "?alt=json-in-script&max-results=4",
                type: "get",
                dataType: "jsonp",
                success: function(a) {
                    b.closest("li").addClass("parent isMega hasSubmenu  menu-item-has-children dropdown");
                    for (var e = 0; e < a.feed.link.length; e++) {
                        var t = a.feed.link[e],
                            s = t.rel,
                            i = t.type;
                        if ("alternate" == s && "text/html" == i) {
                            var r = t.href + "?&max-results=" + perPage;
                            b.attr("href", r)
                        }
                    }
                    var n = a.feed.openSearch$totalResults.$t,
                        l = a.feed.openSearch$startIndex.$t,
                        d = a.feed.openSearch$itemsPerPage.$t;
                    if (4 < n);
                    else;
                    var o = Math.ceil(n / d);
                    console.log(a.feed.entry);
                    if (a.feed.entry) {
                        var c = "<ul";
                        c = c + ' data-itemnums="' + d + '" data-label="' + y + '" data-start="' + l + '" data-stages="' + o + '" data-cstage="1" data-tpst="' + n + '"', c += '><div class="mega-nav"><a class="mega-prev disable" href="javascript:;"><i class="fa fa-arrow-left"></i></a><a class="mega-next" href="javascript:;"><i class="fa fa-arrow-right"></i></a></div><div class="rina-b-mega-loading" style="display:none">  <div class="rina-b-loading-inner"> <div class="rina-loader"></div>  </div>  </div><div class="mega-inner row">';
                        for (e = 0; e < a.feed.entry.length; e++) {
                            var m = a.feed.entry[e],
                                u = a.feed.entry[e].category[0].term,
                                f = a.feed.entry[e].author[0].name.$t;
                            if (m.media$thumbnail) - 1 !== (p = m.media$thumbnail.url).indexOf("/s72-c") && (p = p.replace("/s72-c", "/w750-h450-p-k-no-nu")), -1 !== p.indexOf("img.youtube.com") && (p = p.replace("/default.jpg", "/maxresdefault.jpg"));
                            else var p = "https://4.bp.blogspot.com/-wPwjv7-YYGY/Wc98wlDT8qI/AAAAAAAAAEE/mH8YkPl8qJAH9FMuFKcShQvXXYMmVyrIgCLcBGAs/s1600/notfound.png";
                            for (var v = m.title.$t, h = 0; h < m.link.length; h++)
                                if ("alternate" == m.link[h].rel) var g = m.link[h].href;
                            c = c + '<div class="rina-b-mega-item col-sm-3"><div class="rina-b-m-thumb" style="background:url(\'' + p + "')\"><span class='post-cat'><a href='/search/label/" + u + "?&amp;max-results=" + perPage + "' title='View all posts in " + u + "'>" + u + '</a></span><a class="linkcover" href="' + g + "\"></a></div><div class='entry-header'><div class='article-meta'><span class='posted-on'><i class='fa fa-pencil'></i><a href='" + g + "' rel='bookmark'>" + f + '</a></span></div></div><div class="rina-b-text"><a href="' + g + '">' + v + "</a></div></div>"
                        }
                        c += "</div></ul>", b.after(function() {
                            $(this).after(c);
                            var a = $(this).closest(".isMega");
                            a.find(".mega-prev").click(function() {
                                var o = $(this).closest("ul");
                                o.find(".rina-b-mega-loading").fadeIn(0);
                                var a = o.attr("data-label"),
                                    c = Number(o.attr("data-start")),
                                    m = Number(o.attr("data-cstage")),
                                    e = (Number(o.attr("data-stages")), Number(o.attr("data-itemnums")));
                                if (c -= e, m - 1 <= 1 ? $(this).addClass("disable") : $(this).removeClass("disable"), o.find(".mega-next").removeClass("disable"), 1 < m) {
                                    var u = "",
                                        t = "/feeds/posts/default/-/" + a + "?alt=json-in-script&start-index=" + c + "&max-results=" + e;
                                    $.ajax({
                                        url: t,
                                        type: "get",
                                        dataType: "jsonp",
                                        success: function(a) {
                                            if (o.attr("data-start", c), o.attr("data-cstage", m - 1), a.feed.entry) {
                                                for (var e = 0; e < a.feed.entry.length; e++) {
                                                    var t = a.feed.entry[e],
                                                        s = a.feed.entry[e].category[0].term,
                                                        i = a.feed.entry[e].author[0].name.$t;
                                                    if (t.media$thumbnail) - 1 !== (r = t.media$thumbnail.url).indexOf("/s72-c") && (r = r.replace("/s72-c", "/w750-h450-p-k-no-nu")), -1 !== r.indexOf("img.youtube.com") && (r = r.replace("/default.jpg", "/maxresdefault.jpg"));
                                                    else var r = "https://4.bp.blogspot.com/-wPwjv7-YYGY/Wc98wlDT8qI/AAAAAAAAAEE/mH8YkPl8qJAH9FMuFKcShQvXXYMmVyrIgCLcBGAs/s1600/notfound.png";
                                                    for (var n = t.title.$t, l = 0; l < t.link.length; l++)
                                                        if ("alternate" == t.link[l].rel) var d = t.link[l].href;
                                                    u = u + '<div class="rina-b-mega-item col-sm-3"><div class="rina-b-m-thumb" style="background:url(\'' + r + "')\"><span class='post-cat'><a href='/search/label/" + s + "?&amp;max-results=" + perPage + "' title='View all posts in " + s + "'>" + s + '</a></span><a class="linkcover" href="' + d + "\"></a></div><div class='entry-header'><div class='article-meta'><span class='posted-on'><i class='fa fa-pencil'></i><a href='" + d + "' rel='bookmark'>" + i + '</a></span></div></div><div class="rina-b-text"><a href="' + d + '">' + n + "</a></div></div>"
                                                }
                                                o.find(".mega-inner").html("").html(u), setTimeout(function() {
                                                    o.find(".rina-b-mega-loading").fadeOut()
                                                }, 1e3)
                                            }
                                        },
                                        error: function(a) {
                                            setTimeout(function() {
                                                o.find(".rina-b-mega-loading").fadeOut()
                                            }, 1e3)
                                        }
                                    })
                                }
                            }), a.find(".mega-next").click(function() {
                                var o = $(this).closest("ul");
                                o.find(".rina-b-mega-loading").fadeIn(0);
                                var a = o.attr("data-label"),
                                    c = Number(o.attr("data-start")),
                                    m = Number(o.attr("data-cstage")),
                                    e = Number(o.attr("data-stages")),
                                    t = Number(o.attr("data-itemnums"));
                                if (c += t, m + 1 == e ? $(this).addClass("disable") : $(this).removeClass("disable"), 1 <= m ? o.find(".mega-prev").removeClass("disable") : o.find(".mega-prev").addClass("disable"), m < e) {
                                    var u = "",
                                        s = "/feeds/posts/default/-/" + a + "?alt=json-in-script&start-index=" + c + "&max-results=" + t;
                                    $.ajax({
                                        url: s,
                                        type: "get",
                                        dataType: "jsonp",
                                        success: function(a) {
                                            if (o.attr("data-start", c), o.attr("data-cstage", m + 1), a.feed.entry) {
                                                for (var e = 0; e < a.feed.entry.length; e++) {
                                                    var t = a.feed.entry[e],
                                                        s = a.feed.entry[e].category[0].term,
                                                        i = a.feed.entry[e].author[0].name.$t;
                                                    if (t.media$thumbnail) - 1 !== (r = t.media$thumbnail.url).indexOf("/s72-c") && (r = r.replace("/s72-c", "/w750-h450-p-k-no-nu")), -1 !== r.indexOf("img.youtube.com") && (r = r.replace("/default.jpg", "/maxresdefault.jpg"));
                                                    else var r = "https://4.bp.blogspot.com/-wPwjv7-YYGY/Wc98wlDT8qI/AAAAAAAAAEE/mH8YkPl8qJAH9FMuFKcShQvXXYMmVyrIgCLcBGAs/s1600/notfound.png";
                                                    for (var n = t.title.$t, l = 0; l < t.link.length; l++)
                                                        if ("alternate" == t.link[l].rel) var d = t.link[l].href;
                                                    u = u + '<div class="rina-b-mega-item col-sm-3"><div class="rina-b-m-thumb" style="background:url(\'' + r + "')\"><span class='post-cat'><a href='/search/label/" + s + "?&amp;max-results=" + perPage + "' title='View all posts in " + s + "'>" + s + '</a></span><a class="linkcover" href="' + d + "\"></a></div><div class='entry-header'><div class='article-meta'><span class='posted-on'><i class='fa fa-pencil'></i><a href='" + d + "' rel='bookmark'>" + i + '</a></span></div></div><div class="rina-b-text"><a href="' + d + '">' + n + "</a></div></div>"
                                                }
                                                o.find(".mega-inner").html("").html(u), setTimeout(function() {
                                                    o.find(".rina-b-mega-loading").fadeOut()
                                                }, 1e3)
                                            }
                                        },
                                        error: function(a) {
                                            setTimeout(function() {
                                                o.find(".rina-b-mega-loading").fadeOut()
                                            }, 1e3)
                                        }
                                    })
                                }
                            })
                        })
                    }
                },
                error: function(a) {}
            })
        }
    })
});